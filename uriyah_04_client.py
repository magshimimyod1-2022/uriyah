import socket

ADDR = "127.0.0.1"
LISTEN_PORT = 42075

def setup_client_side():
    """
    function that creates the client socket
    input: none
    output: client socket
    """
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (ADDR, LISTEN_PORT)
    sock.connect(server_address) #connect to the server
    return sock

def client_side(client_msg, sock):
    """
    function that sends the client message to the server
    input: client message, client socket
    output: server message
    """
    sock.sendall(client_msg.encode())
    server_msg = sock.recv(1024)
    server_msg = server_msg.decode()
    print(server_msg)
    sock.close()

def messages_from_server():
    """
    function that receives the server message
    input: none
    output: server message
    """
    sock = setup_client_side()
    client_msg = input("Enter a message: ") #input of the client for the server
    client_side(client_msg, sock)

def main():
    try:
        sock = setup_client_side()
        client_side("Hello", sock)
        while True:
            messages_from_server()
    except ConnectionRefusedError: #if the server is closed
        print("Connection refused")

if __name__ == "__main__":
    main()