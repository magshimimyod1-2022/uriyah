import socket
from datetime import datetime
PROXY_ADDR = "127.0.0.1"
SERVER_PORT = 92
LISTEN_PORT = 9090
SERVER_ADDR = "54.71.128.194"


def send_to_client(server_msg_proxy, client_soc):
    """
    function that sends the server message to the client
    input: server message, client socket
    output: none
    """
    msg = server_msg_proxy
    client_soc.sendto(msg.encode(), (PROXY_ADDR, SERVER_PORT))
        
def server_side():
    """
    function that creates the server socket and listens to the client
    input: none
    output: client socket, listening socket
    """
    listening_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = ('', LISTEN_PORT)
    listening_sock.bind(server_address)
    listening_sock.listen(1) #listen to the client
    client_soc, client_address = listening_sock.accept() #accept the client's connection
    return client_soc, listening_sock

def receive_from_client(client_soc):
    """
    function that receives the data from the client
    input: client socket
    output: client message
    """
    client_msg = client_soc.recv(1024)
    client_msg = client_msg.decode() #convert to string 
    print(client_msg)
    return client_msg

def setup_client_side():
    """
    function that creates the client socket
    input: none
    output: client socket
    """
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (SERVER_ADDR, SERVER_PORT)
    sock.connect(server_address) #connect to the server
    return sock

def client_side(client_msg, sock):
    """
    function that sends the client message to the server
    input: client message, client socket
    output: server message
    """
    sock.sendall(client_msg.encode())
    server_msg = sock.recv(1024)
    server_msg = server_msg.decode()
    sock.close()
    index_msg = server_msg.find("name:\"") #find the index of the name
    if index_msg != -1: #if the name is found
        index_msg += len("name:\"") #move the index to the start of the name
        server_msg = server_msg[:index_msg] + "proxy" + server_msg[index_msg:] #add proxy to the message
    img_index = server_msg.find("jpg")
    if img_index != -1:
        server_msg= server_msg[:img_index] + "." + server_msg[img_index:] #fix the jpg bug by adding a dot
    if server_msg.find("SERVERERROR") != -1:
        index_hashtag = server_msg.find("#") 
        server_msg = "ERROR" + server_msg[index_hashtag:] #add the error to the message
    france_index = server_msg.find("France")
    if france_index != -1: #if the message contains France
        server_msg = "ERROR#\"France is banned!\""
    print(server_msg)
    return server_msg

def check_client_msg(client_msg):
    """
    function that checks if the client message is valid
    input: client message
    output: error message or none
    """
    LEN_OF_YEAR = 4
    this_year = datetime.now().year
    country_index = client_msg.find("&country:")
    year_index = client_msg.find("year:")
    start_of_year = year_index + len("year:")
    end_of_starting_year = client_msg[year_index + len("year:"):country_index].find("-") #find the end of the first year in the message
    if end_of_starting_year != -1:
        year = int(client_msg[start_of_year:start_of_year + end_of_starting_year]) #get the first year
        if year > this_year:
            error_client_msg = "ERROR#\"Year is not valid!\""
            return error_client_msg
    start_of_end_year = start_of_year + end_of_starting_year + 1 #find the start of the second year in the message
    end_of_end_year = start_of_end_year + LEN_OF_YEAR
    if end_of_end_year != -1:
        year = int(client_msg[start_of_end_year:end_of_end_year]) #get the second year
        if year > this_year:
            error_client_msg = "ERROR#\"Year is not valid!\""
            return error_client_msg
def main():  

    while True: #loop forever untill the program is closed
        client_soc, listening_sock = server_side()
        sock = setup_client_side()
        client_msg = receive_from_client(client_soc)
        check_client_msg(client_msg)
        if check_client_msg(client_msg) == None: #if the message is valid
            server_msg = client_side(client_msg, sock)
        else:
            server_msg = check_client_msg(client_msg)
        send_to_client(server_msg, client_soc)
        client_soc.close()
        listening_sock.close()

if __name__ == "__main__":
    main()