import socket

ADDR = "127.0.0.1"
LISTEN_PORT = 42075
DEFAULT_CLIENT_GET_MSG = "GET msg received"
DEFAULT_CLIENT_MENU_MSG = "MENU msg received"
WELCOME_MSG = "Welcome"

def send_to_client(msg, client_soc):
    """
    function that sends the server message to the client
    input: server message, client socket
    output: none
    """
    client_soc.sendall(msg.encode())


def server_side():
    """
    function that creates the server socket and listens to the client
    input: none
    output: client socket, listening socket
    """
    listening_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = ('', LISTEN_PORT)
    listening_sock.bind(server_address)
    listening_sock.listen(1) #listen to the client
    client_soc, client_address = listening_sock.accept() #accept the client's connection
    return client_soc, listening_sock

def receive_from_client(client_soc):
    """
    function that receives the data from the client
    input: client socket
    output: client message
    """
    client_msg = client_soc.recv(1024)
    client_msg = client_msg.decode() #convert to string 
    return client_msg

def main():
    run = True
    while run:
        client_soc, listening_sock = server_side()
        client_msg = receive_from_client(client_soc) 
        if client_msg == "Hello": #if the client connects, send the welcome message
            send_to_client(WELCOME_MSG, client_soc)
        elif client_msg == "GET":
            send_to_client(DEFAULT_CLIENT_GET_MSG, client_soc)
        elif client_msg == "MENU":
            send_to_client(DEFAULT_CLIENT_MENU_MSG, client_soc)
        elif client_msg == "QUIT":
            run = False #run untill quit is received
        client_soc.close()
        listening_sock.close()
    client_soc.close()
    listening_sock.close()

if __name__ == "__main__":
    main()
